resource "google_storage_bucket" "COLDLINE" {
  name     = "prod-kubernetes-project"
  storage_class = "COLDLINE"
  location = "us-east4"
  }

resource "google_storage_bucket_object" "text" {
  name = "prod-kubernetes-texte"
  content = "*** Welcome to my first text file contents"
  storage_class = "COLDLINE"
  bucket = "${google_storage_bucket.COLDLINE.name}"
  }