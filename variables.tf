variable "project" {
  default = "qa-project-20200"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}

variable "cluster" {
  default = "prod-cluster"
}

variable "name" {
  default = "prod-cluster"
}

variable "username" {
  default = "k8sadmin"
}

variable "password" {
  default = "Passwd@123Passwd@123"
}  
