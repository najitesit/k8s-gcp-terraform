resource "google_container_cluster" "default" {
  name        = "${var.cluster}-1"
  project     = "${var.project}"
  description = "Demo GKE Cluster"
  location    = "${var.zone}"

  remove_default_node_pool = true
  initial_node_count = 1
  
  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "default" {
  name       = "${var.name}-node-pool"
  project     = "${var.project}"
  location   = "${var.zone}"
  cluster    = "${google_container_cluster.default.name}"
  node_count = 4

  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }
    // metadata_startup_script = "${file("./huginn.yml")}"

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      ]
    }
  }