provider "google" {
  credentials = "${file("creds/qa-project-20200.json")}"
  project     = "QA-project-20200"
  region      = "${var.region}"
}
