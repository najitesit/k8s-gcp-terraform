terraform {
  backend "s3" {
    bucket = "devops-bucket-01"
    key    = "qa-project-20200.state"
    //key    = "terraform-state-file"
    region = "us-east-1"
    encrypt = "true"
    profile = "default"
  }
}
